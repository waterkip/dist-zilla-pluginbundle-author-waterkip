#
# It is not intended to be small or super nifty, it tries to cache some,
# but it is intended to be easy to go into an environment and poke
# around and edit and less things
#
FROM registry.gitlab.com/opndev/perl5/docker-p5/moosy-development:latest

COPY cpanfile .
RUN docker-cpanm -n Test::Vars \
    && docker-cpanm Net::SSLeay \
    && docker-cpanm Dist::Zilla \
    && docker-cpanm --installdeps . \
    && rm -rf $HOME/.cpanm/

COPY . .
RUN prove -l \
    && docker-cpanm -n . \
    && rm -rf $HOME/.cpanm/

RUN mkdir -p $HOME/.dzil/
COPY corpus/dzil.config /root/.dzil/config.ini
COPY corpus/gitconfig /root/.gitconfig

RUN cd /tmp \
    && dzil new -P Author::WATERKIP foobar

RUN cd /tmp/foobar \
    && dzil build \
    && dzil test \
    && dzil xtest \
    && cd /tmp \
    && rm -rf /tmp/foobar
